package br.com.empresa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ValidacapitalApplication {

	public static void main(String[] args) {
		SpringApplication.run(ValidacapitalApplication.class, args);
	}

}
