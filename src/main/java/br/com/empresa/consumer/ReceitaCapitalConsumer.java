package br.com.empresa.consumer;


import br.com.empresa.models.ReceitaCapital;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.math.BigDecimal;

@Component
public class ReceitaCapitalConsumer {

    @KafkaListener(topics = "spec3-ana-carolina-3", groupId = "Carol-3")
    public void receber(@Payload ReceitaCapital receitaCapital) throws CsvRequiredFieldEmptyException,
            IOException, CsvDataTypeMismatchException {

        if(Double.valueOf(receitaCapital.getCapital_social()) > 100000000.00) {
            generateCsvFile(receitaCapital, true);
        }else{
            generateCsvFile(receitaCapital, false);
        }


        System.out.println("Recebida validacao do cliente:" + receitaCapital.getNome()
                + " com o capital social:  " + receitaCapital.getCapital_social());
    }

    private static void generateCsvFile(ReceitaCapital receitaCapital, boolean isCapitalValido) throws IOException,
            CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {

        Writer writer;
        if(isCapitalValido){
         writer = new FileWriter("capital_valido.csv", true);
        }
        else{
            writer = new FileWriter("capital_recusado.csv", true);
        }

        StatefulBeanToCsv<ReceitaCapital> beanToCsv =  new StatefulBeanToCsvBuilder(writer).build();

        beanToCsv.write(receitaCapital);

        writer.flush();
        writer.close();
    }
}
